import os 
import random
import json
import numpy as np
from PIL import Image
import PIL
import torch
from torchvision import transforms

def read_config(file):
    with open(file, "r") as f:
        config=json.load(f)
    return config

def read_data(file):
    """
    Read csv file to extract a corresponding id and image.
    Input: 
        file
    Return:num_random
        id_to_img: dict, keys are ids, values are images
        img_to_id: dict, keys are images, values are ids
    """
    with open(file, "r") as f:
        f.readline()
        id_to_img={}
        img_to_id={}
        for line in f:
            splitted_line=line.strip().split(",")
            img=splitted_line[0]
            id=splitted_line[1]
            img_to_id[img]=id
            if id not in id_to_img:
                id_to_img[id]=[]
            id_to_img[id].append(img)
    return id_to_img, img_to_id



def ids_over_n_imgs(id_to_img,n):
    """
    Find list of ids having more than n images.
    """
    ids=[id for id in id_to_img if len(id_to_img[id])>=n]
    return ids

def remove_id(id_to_img, ids_to_remove=[]):
    """
    To remove ids in a list.
    Return dicts corresponding id-image.
    """
    id_to_img_new={id: img for id, img in id_to_img.items() if id not in ids_to_remove}
    return id_to_img_new

def convert_img2id(id_to_image):
    """
    Convert a dictionary id_img to a dictionary img_id
    """
    img_to_id ={}
    for id in id_to_image:
        for img in id_to_image[id]:
            img_to_id[img]=id
    return img_to_id


def select_imgs(ids_to_select, id_to_images,num_per_id):
    """
    Select images from a list of ids.
    Return dict. 
    """
    id_to_img_select={}
    for id in ids_to_select:
        images = id_to_images[id]
        assert len(images)>= num_per_id
        random.shuffle(images)
        id_to_img_select[id]=images[:num_per_id]
    return id_to_img_select




def select_matched_pairs(images, num_each_img):
    """
    Select all matched pairs from a list of images. 
    Return a list of matched pairs.
    """
    assert sum(num_each_img.values())==(len(images)-1)*len(images)/2
    num=len(images)
    return [[images[i], images[(i+j+1)%num]] for i in range(num)\
            for j in range(num_each_img[images[i]])]

def num_each_img_(images):
    """
    Calculate the number of matched pairs corresponding to each image in list images.
    Return list of numbers.
    """
    num=len(images)
    if num==0:
        return []
    num_each_img={ img: (num-1)//2 for img in images }
    surplus=(num*(num-1))/2 - sum(num_each_img.values())
    if surplus>0:
        for i, img in enumerate(images):
            if i<surplus:
                num_each_img[img]+=1
            else:
                return num_each_img
    return num_each_img


def img_to_id_num(id_to_img, num_each_img):
    """
    Convert dict id_to_img and dict num_each_img to list list_imgs and list list_num
    """
    list_imgs=[]
    list_num=[]
    for id in id_to_img:
        for img in id_to_img[id]:
            list_imgs +=[img]
            list_num +=[num_each_img[id][img]]
    return list_imgs, list_num

def img_to_tensor(list_imgs, path_file, transform=None, size=(224,224)):
    """
    list_imgs: list of names of images.

    """
    imgs_tensor=[]
    for img in list_imgs:
        path=os.path.join(path_file,img)
        img_tensor=Image.open(path)
        if transform==None:
            img_tensor=img_tensor.resize(size, PIL.Image.ANTIALIAS)
            if len(np.shape(img_tensor))==2:
                img_tensor=np.stack([img_tensor,img_tensor,img_tensor], axis=-1)
                img_tensor = np.transpose(img_tensor, [2, 0, 1])
        else: 
            img_tensor=transform(img_tensor)
        
        imgs_tensor.append(img_tensor)
    imgs_tensor=np.stack(imgs_tensor,axis=0)
    imgs_tensor=torch.tensor(imgs_tensor, dtype=torch.float32)

    return imgs_tensor


def one_img_to_tensor(img, path_file, transform=None, size=(224,224)):
    """
    img: name of image.

    """
    path=os.path.join(path_file,img)
    img_tensor=Image.open(path)
    if transform==None:
        img_tensor=img_tensor.resize(size, PIL.Image.ANTIALIAS)
        if len(np.shape(img_tensor))==2:
            img_tensor=np.stack([img_tensor,img_tensor,img_tensor], axis=-1)
            img_tensor = np.transpose(img_tensor, [2, 0, 1])
    else: 
        img_tensor=transform(img_tensor)
    
        
    img_tensor=torch.tensor(img_tensor, dtype=torch.float32)

    return imgs_tensor

    





    
    