### Training strategy:

import numpy as np
import torch
import torch.optim as optim
import torch.nn as nn
import os
from PIL import Image
import PIL
import argparse
from model import ModelBranch,ModelHead, Loss, ModelFull
from loader import SiameseDataset, ValDataset
import utils
from validation import Validation

from tensorboardX import SummaryWriter

from torchvision import transforms

transform=transforms.Compose([
    transforms.Grayscale(),
    transforms.ColorJitter(brightness=0.5, contrast=0.1, saturation=0.4, hue=0.1),
    transforms.RandomRotation(degrees=(0, 10),expand=True),
    transforms.Resize(size=(224,224), interpolation=2),
    transforms.ToTensor(),
    transforms.Normalize(mean=[0.5475962025269793], std=[0.26555964702352214]),
          ])

class SiameseTrainer():
    def __init__(self, config):
        self.config=config

        self.loader=SiameseDataset(self.config["data"]["train_csv"],\
                                    self.config["loader"]["num_ids"],\
                                    self.config["loader"]["num_img_per_id"],\
                                    self.config["loader"]["batch_size"],
                                    transform=transform)
        self.loader_val=ValDataset(self.config["val"]["val_csv"],\
                    self.config["val"]["batch_size"],transform=transform)
     #   self.batch_gen_val=self.loader_val.batch_gen()
        self.select_ids_gen=self.loader.select_ids_gen()
        self.matched_pairs_gen=self.loader.matched_pairs_gen()
        self.unmatched_pairs_gen=self.loader.unmatched_pairs_gen()

        self.num_iteration = self.config["train"]["max_step"]
        self.batch_size = self.config["train"]["batch_size"] 
        assert self.batch_size % 2 == 0
        self.half_batch_train=self.batch_size//2
        self.loss=Loss()
        self.model_branch=ModelBranch(resnet=config["train"]["resnet"])
        
        self.model_head=ModelHead()
        self.step=0
        
        self.use_gpu = torch.cuda.is_available()
        self.device_()
        self.model_branch.to(self.device)
        self.model_head.to(self.device)
        self.loss.to(self.device)

        # self.optimizer=optim.Adam(list(self.model_branch.parameters())+
        #                         list(self.model_head.parameters()),
        #                         lr=self.config["train"]["init_lr"],
        #                         weight_decay=self.config["train"]["weight_decay"])
        self.optimizer=optim.Adam(self.model_branch.parameters(),
                                lr=self.config["train"]["init_lr"],
                                weight_decay=self.config["train"]["weight_decay"])
        
        self.load_checkpoint()

        self.make_label()
    
        self.val=Validation(config=self.config, margin=1,\
                        dataset=self.loader_val, device=self.device)
        self.val_every=self.config["val"]["val_every"]

        self.writer=SummaryWriter(log_dir=self.config["train"]["board_X"])
        self.graph_summary()

    
    def device_(self):
        if self.use_gpu:
            self.device='cuda'
        else:
            self.device='cpu'

    def train(self):
        loss_contras_best= 1e+10
        loss_avg=0
        for step in range(self.step, self.step+self.num_iteration):
            batch0=[]
            batch1=[]  
        
            for _ in range(self.half_batch_train):
                if self.loader.matched_pairs==[]:
                    list_ids=next(self.select_ids_gen)
                    id_to_img=self.loader.select_list_imgs(list_ids)
                    img_to_num=self.loader.num_each_img(id_to_img)
                   
                    self.loader.matched_pairs_(id_to_img,img_to_num)
                    if self.loader.num_batch==0: 
                        self.loader.naive_unmatched_pairs_(id_to_img,img_to_num)
                    else:
                        list_imgs, list_num=utils.img_to_id_num(id_to_img, img_to_num)
                        scores_mtx=self.scores_matrix_modified(list_imgs)
                        self.loader.hard_unmatched_pairs_modified(list_imgs, list_num, scores_mtx)
                pair_match=next(self.matched_pairs_gen)
                pair_unmatch=next(self.unmatched_pairs_gen)
                batch0+=[pair_match[0],pair_unmatch[0]]
                batch1+=[pair_match[1],pair_unmatch[1]]
            

            batch0=utils.img_to_tensor(batch0,path_file=self.config["data"]["train_data"],\
                                transform=self.loader.transform).to(self.device)
            batch1=utils.img_to_tensor(batch1,path_file=self.config["data"]["train_data"],\
                                transform=self.loader.transform).to(self.device)
            batch0=self.model_branch.forward(batch0)
            batch1=self.model_branch.forward(batch1)
            scores=self.model_head.forward(batch0, batch1)
        
            loss=self.loss(scores, self.label)
            self.optimizer.zero_grad()
            loss.backward()
            self.optimizer.step()
            
            loss_avg+=loss.item()

            if step%self.config["train"]["print_every"]==0:
                print("Step: {0} - loss_avg: {1}".format(step, loss.item()))
                
                
            if (step+1)%self.val_every==0:
            #    self.val.val(self.model_branch)
                correct_count, loss_contrast =self.val.val(self.model_branch)
                loss_avg/=self.val_every
                self.writer.add_scalars("Contrastive loss", {"val": loss_contrast, \
                                    "train": loss_avg},\
                                    global_step=step)  
                loss_avg=0

                if loss_contrast<loss_contras_best:
                    self.save_checkpoint(step,correct_count)
                    loss_contras_best=loss_contrast


            if (step+1)%self.config["train"]["reduce_lr_every"]==0:
                for g in self.optimizer.param_groups:
                    g["lr"]*=self.config["train"]["reduce_lr_rate"]
                    print("Reduce learning rate. Current lr: {0}".format(g["lr"]))
            
            

    def make_label(self):
        self.label = torch.tensor([1., 0.] * self.half_batch_train,\
                                        dtype=torch.float32,\
                                        device=self.device)

    # def scores_matrix_(self,list_imgs):
    #     list_imgs=utils.img_to_tensor(list_imgs,\
    #             path_file=self.config["data"]["train_data"]).to(device='cpu')
    #     if self.device=="cuda":
    #         self.model_branch.to(device='cpu')
    #         self.model_head.to(device='cpu')
    #     features=self.model_branch(list_imgs)
    #     scores_matrix=self.model_head(features.repeat(self.loader.batch_size,1),\
    #         features.repeat(1,self.loader.batch_size).resize(features.size()[0]*\
    #         self.loader.batch_size,features.size()[1]))
    #     scores_matrix=scores_matrix.resize(self.loader.batch_size,self.loader.batch_size)
    #     self.model_branch.to(device=self.device)
    #     self.model_head.to(device=self.device)
    #     return scores_matrix
    
    def scores_matrix_modified(self,list_imgs):
        print("calculate score matrix")
        list_imgs=utils.img_to_tensor(list_imgs,\
                path_file=self.config["data"]["train_data"],
                transform=self.loader.transform).to(device='cpu')
        if self.device=="cuda":
            self.model_branch.to(device='cpu')
        
            self.model_head.to(device='cpu')
        with torch.no_grad():
            features=self.model_branch(list_imgs)
        
        upper_idx=np.triu_indices(len(list_imgs),k=1)
        f1=features[upper_idx[0],:]
        f2=features[upper_idx[1],:]


        scores_matrix=self.model_head(f1,f2)
        
        self.model_branch.to(device=self.device)
        self.model_head.to(device=self.device)
        return scores_matrix 

    def save_checkpoint(self, step, correct_count):
        torch.save({
            "step": step,
            "state_dict": self.model_branch.state_dict(),
            "optimizer": self.optimizer.state_dict(),
            "correct_count": correct_count
        }, self.config["check_point"])

    def load_checkpoint(self):
        try:
            print("Load check point.")
            checkpoint = torch.load(self.config["check_point"])
            self.model_branch.load_state_dict(checkpoint['state_dict'])
            self.optimizer.load_state_dict(checkpoint['optimizer'])
            self.step=checkpoint["step"]
        except:
            print("Cannot load check point, start at the beginning.")
        return self.model_branch

    def graph_summary(self):
        dummy_input=torch.Tensor(1,3,224,224)
        model=ModelFull()

        self.writer.add_graph(model, (dummy_input,dummy_input))

def train():
    arg_parser = argparse.ArgumentParser(description="")
    arg_parser.add_argument(
            '--config',
            default='./config.json',
            help='The Configuration file in json format')
    
    args = arg_parser.parse_args()

    config = utils.read_config(args.config)

    trainer = SiameseTrainer(config)
    # agent.validate()
    trainer.train()


if __name__ == "__main__":
    train()
    