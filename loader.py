import utils
import torch
from  torch.utils.data import Dataset, DataLoader

import random
import numpy as np


class SiameseDataset():
    """
    Dataset for siamese net. Contains pairs of images.
    A pair consists of two images, either of the same or of different whales. 
    A batch contains the same number of matched and of unmatched pairs. Each unmatched 
    pair contains one image from corresponding matched pair.
    Select all possible matched pairs. 
    Unmatched pairs are chosen such that their scores are as small as possible, i.e their two 
    images are hard to distinguish.
    Unmatched pairs are chosen after a certain training steps, used trained model to calculate 
    scores then use the scores to choose unmatched pairs.
    """
    def __init__(self, file_csv, num_ids, \
                    num_img_per_id, batch_size, transform=None):
        id_to_img, self.img_to_id = utils.read_data(file_csv)
        self.id_to_img=utils.remove_id(id_to_img, ["new_whale"])
        self.ids_over_n_imgs=utils.ids_over_n_imgs(self.id_to_img, num_img_per_id)
        self.num_ids=num_ids
        self.batch_size=batch_size
        self.num_random_select=batch_size-num_ids*num_img_per_id
        self.num_img_per_id=num_img_per_id
        self.num_batch=-1
        self.transform=transform

        self.matched_pairs=[]
        self.unmatched_pairs=[]

        
    def select_list_imgs(self, selected_ids):
        """
        Select all images for a batch.
        Return a dict.
        """
        id_to_img1=utils.select_imgs(selected_ids,self.id_to_img, self.num_img_per_id)
        id_to_img2=self.random_select_batch(selected_ids)
        id_to_img1.update(id_to_img2)
        return id_to_img1

    
    def random_select_batch(self, ids_to_select):
        """
        Select num_random images with ids different from ids_to_select.
        Return: dict  
        """
        id_to_img_removed= utils.remove_id(self.id_to_img, ids_to_select)
        images=[]
        for id in id_to_img_removed:
            images+=id_to_img_removed[id]
        random.shuffle(images)
        selected_images=images[:self.num_random_select]
        
        id_to_img_select={}
        
        for img in selected_images:
            id=self.img_to_id[img]
            if id not in id_to_img_select:
                id_to_img_select[id]=[]
            id_to_img_select[id].append(img)

        return id_to_img_select


    def num_each_img(self, id_to_img):
        """
        Calculate number of pairs for each image.
        Input: 
            id_to_img: dict {id:imgs} 
        Return a dict.
        """
        num={}
        for id in id_to_img:
            num[id]=utils.num_each_img_(id_to_img[id])
        return num


    def matched_pairs_(self, id_to_img,num_each_img):
        """
        Select all matched pairs.
        Input:
            id_to_img: dict {id:imgs}
            num_each_img: dict {id: {img:num}}
        Return: matched_pairs: list [[img1,img2]]
        """
        for id in id_to_img:
            images=id_to_img[id]
            matched_pairs=utils.select_matched_pairs(images, num_each_img[id])
            self.matched_pairs+=matched_pairs
        

    def naive_unmatched_pairs_(self, id_to_img, num_each_img):
        """
        Naively select unmatched pairs.
        Input:
            id_to_img: dict {id:img}
            num_each_img: {id:{img:num}}
        Return: unmatched_pairs: list [[img1, img2]]
        """
        for id in id_to_img:
            imgs=[]
            for i in id_to_img:
                if i!=id:
                    imgs+=id_to_img[i]
            for i, img in enumerate(id_to_img[id]):
                random.shuffle(imgs)
                paired_imgs=imgs[:num_each_img[id][img]]
                self.unmatched_pairs +=[[img,img1] for img1 in paired_imgs]

    def hard_unmatched_pairs_(self, list_imgs, list_num, scores_mtx):
        """
        Select hard nummatche_pairs.
        Input: 
            list_imgs: list [img]
            list_num: list [num]
            scores_matrix: tensor matrix of scores
        Return: unmatched_pairs: list [[img1,img2]]
        """
        for i, img in enumerate(list_imgs):
            idx=[i for i, j in enumerate(list_imgs)\
                            if j not in self.id_to_img[self.img_to_id[img]]]
            scores=scores_mtx[i]
            scores=[scores[j] for j in idx]
            num=max(list_num[i]*1.3, list_num[i]+3)
            if num<len(idx):
                top_idx=np.argpartition(scores, num)[:num]
                random.shuffle(top_idx)
                select_idx=top_idx[:list_num[i]]
            elif len(idx)>=list_num[i]:
                top_idx=np.arange(len(idx))
                random.shuffle(top_idx)
                select_idx=top_idx[:list_num[i]]
            else:
                remained=list_num[i]-len(idx)
                random_idx=np.random.choice(idx, size=remained)
                select_idx=np.concatenate(idx, random_idx)
            
            self.unmatched_pairs+=[[img, im] for i, im in enumerate(list_imgs)\
                            if i in select_idx]

        
    def hard_unmatched_pairs_modified(self, list_imgs, list_num, scores_mtx):
        """
        Select hard nummatche_pairs using modified calculation of scores_mtx.
        Input: 
            list_imgs: list [img]
            list_num: list [num]
            scores_matrix: tensor matrix of scores of upper pairs.
        Return: unmatched_pairs: list [[img1,img2]]
        """
        triu_idx=np.triu_indices(len(list_imgs),k=1)
        for i, img in enumerate(list_imgs):
            idx=[i for i, j in enumerate(list_imgs)\
                            if j not in self.id_to_img[self.img_to_id[img]]]
            idx_mtx=np.any([triu_idx[0]==i, triu_idx[1]==i],axis=0)
            idx_mtx= np.squeeze(np.argwhere(np.asarray(idx_mtx)))
            scores=scores_mtx[idx_mtx]
            scores=torch.cat((scores[:i],torch.tensor([0.0]),scores[i:]),0)
            scores=[scores[j] for j in idx]
            num=max(list_num[i]*1.3, list_num[i]+3)
            if num<len(idx):
                top_idx=np.argpartition(scores, num)[:num]
                random.shuffle(top_idx)
                select_idx=top_idx[:list_num[i]]
            elif len(idx)>=list_num[i]:
                top_idx=np.arange(len(idx))
                random.shuffle(top_idx)
                select_idx=top_idx[:list_num[i]]
            else:
                remained=list_num[i]-len(idx)
                random_idx=np.random.choice(idx, size=remained)
                select_idx=np.concatenate(idx, random_idx)
            

            self.unmatched_pairs+=[[img, im] for i, im in enumerate(list_imgs)\
                            if i in select_idx]

    def select_ids_gen(self):
        """
        Generator to generate list of ids.
        """
        num_batches_per_epoch=len(self.ids_over_n_imgs)//self.num_ids
        step=num_batches_per_epoch
        while True:
            if step==num_batches_per_epoch:
                step=0
                random.shuffle(self.ids_over_n_imgs)
            selected_ids=self.ids_over_n_imgs[step*self.num_ids:(step+1)*self.num_ids]
            step+=1
            self.num_batch+=1
            yield selected_ids

    
    def matched_pairs_gen(self):
        """
        Generators to generate a matched pair.
        """
        while True:
            yield self.matched_pairs.pop()

    def unmatched_pairs_gen(self):
        """
        Generators to generate an unmatched pair.
        """
        while True:
            yield self.unmatched_pairs.pop()     


#Validation loader
class ValDataset():
    """
    Dataset for validation. Contains batches of pairs of images.
    Each pair consists of either two same id images (matched) or different id images (unmatched).
    Each batch has one matched pair at the first position, the unmatched pairs at all the rest
    positions.
    
    """
    def __init__(self, val_file, batch_val, transform=None):
        self.id_to_img, self.img_to_id= utils.read_data(val_file)
        self.ids_over_2=utils.ids_over_n_imgs(self.id_to_img,2)
        self.batch_val=batch_val
        self.transform=transform
        self.make_batches()
    def make_batches(self):
        self.batches=[]
        for img, id in self.img_to_id.items():
            if id in self.ids_over_2:
                ims=[im for im in self.id_to_img[id] if im !=img]
                im=np.random.choice(ims)
                ims=[im for im, i in self.img_to_id.items() if i !=id]
                random.shuffle(ims)
                ims=[im]+ims[:(self.batch_val-1)]
                self.batches+=[[img]+[im for im in ims]]
    
    
    def __getitem__(self,idx):
        return  self.batches[idx]

    def __len__(self):
        return len(self.batches)
                 
    

                

    

        




    
