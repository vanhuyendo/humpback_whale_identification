import torch.nn as nn
import torch.nn.functional as F
import torch
import torchvision.models as models

def flatten(x):
    N = x.shape[0] # read in N, C, H, W
    return x.view(N, -1)  # "flatten" the C * H * W values into a single vector per image

class Flatten(nn.Module):
    def forward(self, x):
        return flatten(x)

class ModelBranch(nn.Module):
    """
    Model from https://www.cs.cmu.edu/~rsalakhu/papers/oneshot1.pdf
    """
    def __init__(self, resnet=None):
        super(ModelBranch, self).__init__()
        if resnet!=None:
            self.branch_model=models.resnet18()
            self.branch_model.conv1=nn.Conv2d(1, 64, kernel_size=7, stride=2, padding=3,
                               bias=False)
        else:
            self.branch_model=nn.Sequential(
                        nn.Conv2d(3,64, kernel_size=7, padding=3),
                        nn.ReLU(),
                        nn.MaxPool2d(kernel_size=2),
                        nn.Conv2d(64,128, kernel_size=3, padding=1),
                        nn.ReLU(),
                        nn.MaxPool2d(kernel_size=2),
                        nn.Conv2d(128, 256, kernel_size=(3, 3),padding=1),
                        nn.ReLU(),
                        nn.MaxPool2d(kernel_size=2),
                        nn.Conv2d(256, 256, kernel_size=(3, 3),padding=1),
                        nn.ReLU(),
                        nn.MaxPool2d(kernel_size=2),
                        nn.Conv2d(256, 256, kernel_size=(3, 3), padding=1),
                        nn.ReLU(),
                        nn.MaxPool2d(kernel_size=2),
                        Flatten(),
                        nn.Linear(256*7*7,1000)
                        )
    
    
    def forward(self, x):
        output=self.branch_model(x)
        return output


class ModelHead(nn.Module):
    """
    Calculate distance of two features.
    """
    def __init__(self):
        super(ModelHead, self).__init__()
#        self.linear=nn.Linear(1000,1)
    def forward(self, x1, x2):
#        diff=torch.abs(x1-x2)
#        score=torch.sigmoid(self.linear(diff))    
        scores=torch.sum(torch.pow(x1-x2,2),1)
        return scores


class Loss(nn.Module):
    """
    Contrastive loss by http://yann.lecun.com/exdb/publis/pdf/hadsell-chopra-lecun-06.pdf.
    """
    def __init__(self,margin=1):
        super(Loss, self).__init__()
        self.margin=margin
    def forward(self, score, label):
        mdist=torch.clamp(self.margin-score,min=0)
        loss=(1-label)*torch.pow(score,2)+\
                label*torch.pow(mdist,2) 
        loss=torch.sum(loss)/len(score)   
        return 0.5*loss


class ModelFull(nn.Module):
    """
    Model includes branch model and head model.
    """
    def __init__(self):
        super(ModelFull, self).__init__()
        self.branch=ModelBranch()
        self.head=ModelHead()
   
    def forward(self, x1, x2):
        out1=self.branch(x1)
        out2=self.branch(x2)
        score=self.head(out1,out2)
        return score




