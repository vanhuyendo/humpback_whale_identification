from loader import ValDataset
import utils
from model import ModelHead, Loss
import torch
import numpy as np

class Validation():
    """
    One batched is correctly predicted if the matched pair has the smallest scores.
    The accuracy is the number of corrected prediction batches.
    """
    def __init__(self, config, device,margin, dataset):
        self.config=config
        self.dataset=dataset
        self.transform=self.dataset.transform

        self.batch_val=self.config["val"]["batch_size"]

        self.margin=margin
        self.device=device
        self.make_label()
        self.model_head=ModelHead().to(self.device)
        self.loss=Loss().to(self.device)
    
    
    def make_label(self):
        label = torch.zeros(self.batch_val,dtype=torch.float32)
        label[0]=1
        self.label=label.to(self.device)

    def accuracy_batch(self, scores):
        _,idx=scores.min(0)
        loss=torch.sum(torch.clamp(scores[0]-scores+self.margin,min=0))/(self.batch_val-1)
        if idx==0:
            return 1, loss
        return 0, loss
    
    def val(self, model_branch):
        correct_count=0
        loss_hinge=[]
        loss_contrastive=[]
        with torch.no_grad():
            for idx in range(100):
                batch=self.dataset[idx]
                batch=utils.img_to_tensor(batch,path_file= self.config["data"]["train_data"],
                                    transform=self.transform)
                
                batch = batch.to(device=self.device)
                
                features=model_branch(batch)
                score=self.model_head(features[0].repeat((self.batch_val,1)),features[1:])
                acc, loss_hinge_step=self.accuracy_batch(score)
                correct_count+=acc
                loss_hinge+=[loss_hinge_step.numpy()]
                loss_contrastive+=[self.loss(score, self.label).numpy()]
            loss_hinge=np.mean(loss_hinge)
            loss_contrastive=np.mean(loss_contrastive)
    #    print("There are: {0}/{1} correct prediction".format(correct_count, len(self.dataset)))
        print("There are: {0}/100 correct prediction".format(correct_count))
        print("Hinge loss: {0} - Contrastive loss: {1}" .format(loss_hinge, loss_contrastive))
     
        return correct_count, loss_contrastive
    
    
