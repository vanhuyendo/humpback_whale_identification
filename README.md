# Identify Humpback Whale by Siamese net in Pytorch.

The Siamese net is described in [paper](https://www.cs.cmu.edu/~rsalakhu/papers/oneshot1.pdf)

This project uses pytorch.

Dataset and objective are from [Kaggle challenge Humpback Whale Identification](https://www.kaggle.com/c/humpback-whale-identification) 

### Dependencies
+ python >= 3.6
+ Pytorch 1.0.1
+ PIL
+ heapq
+ numpy

### How to run

```
$ python train.py
```