# input: img
# ouput: 5 whales
# scores(img,whales)=mean/var(scores(img, im) for im in whales)
# compare mean, take 5 min values.
# the fifth > threshold, replace it by new_whale.
# find the threshold? calculate scores(new_whale, the rest)?

import utils
import torch
from torch.utils.data import Dataset, DataLoader
import torch.optim as optim
from model import ModelBranch, ModelHead
import numpy as np
from heapq import nsmallest
from split_csv import dict_to_csv
from train import SiameseTrainer
import argparse
import os
from PIL import Image
import PIL
#import matplotlib.pyplot as plt


class PredictDataset(Dataset):
    """
    Dataset of images. Contain images in tensor.   
    
    """
    def __init__(self, config, transform=None):
        self.data=config["data"]["train_data"]
        _,img_to_id=utils.read_data(config["data"]["train_csv"])
        self.imgs=[img for img in img_to_id]
        self.transform=transform

    def __len__(self):
        return len(self.imgs)

    def __getitem__(self,idx):
        img=self.imgs[idx]
        return utils.one_img_to_tensor(img, self.data, transfrom=self.transform)

def Img_to_Feature(config, branch_model_trained,device='cpu'):
    """
    Save features. Separate new_whales to different folder.
    Input:  config: config
            branch_model_trained: trained model to calculate features
            device: device to calculate features, defautl cpu
    
    """
    dataset=PredictDataset(config, transform=transform)
    
    batch_size=config["prediction"]["batch_size"]
    data= DataLoader(dataset, batch_size=batch_size, shuffle=False)
    branch_model_trained.to(device)
    
    path=config["prediction"]["embedding_folder"]
    with torch.no_grad():
        step=0
        for imgs in data:
            imgs.to(device)
            features=branch_model_trained.forward(imgs).to('cpu')
            imgs=dataset.imgs[step*batch_size: (step+1)*batch_size]
            for i in range(len(imgs)):
                path_img=path+f"{imgs[i]}.pt"
                torch.save(features[i],path_img)
            step+=1




class Features(Dataset):
    """
    Dataset of features. Contain tensors of features.
    """
    def __init__(self, config):
        _,self.img_to_id=utils.read_data(config["data"]["train_csv"])
        self.embedding_folder=config["prediction"]["embedding_folder"]
        self.remove_ids()

    def remove_ids(self, remove=["new_whale"]):
        self.imgs=[]
        for img, id in self.img_to_id.items():
            if id not in remove:
                self.imgs+=[img]
    
    def __len__(self):
        return len(self.imgs)
    
    def __getitem__(self,idx):
        img=self.imgs[idx]
        file=self.embedding_folder+f"{img}.pt"
        return torch.load(file)

class Predictor():
    """
    Predict certain number of potential ids of any image.
    Prediction is done by comparing (i.e computing scores) feature of given image to all ones 
    of other trained images, which are save in features folder. The smaller score is,
    the more likely the image belongs to the id. Select k smallest scores.
    It is asigned to new_whale if the k_th score is larger than a threshold.
    The threshold is computed by percentile of all same group scores.
    """
    def __init__(self, config, branch_model_trained):
        self.config=config
        self.test_data=self.config["data"]["test_data"]
        self.id_to_img, self.img_to_id=utils.read_data(self.config["data"]["train_csv"])
        self.use_gpu = torch.cuda.is_available()
        self.device_()
        self.branch_model=branch_model_trained.to(self.device)
        self.batch_size=self.config["prediction"]["batch_size"]
        self.num_top=self.config["prediction"]["num_top"]
        self.confident_rate=self.config["prediction"]["confident_rate"]
        
    
        self.head_model=ModelHead().to(self.device)
        
        self.dataset=Features(config)
        self.data= DataLoader(self.dataset, batch_size=self.batch_size, shuffle=False)
    #    self.train=SiameseTrainer(config)
        self.Threshold()
        print(self.threshold)
        
    
    def device_(self):
        if self.use_gpu:
            self.device='cuda'
        else:
            self.device='cpu'
    
    def Threshold(self):
        """
        Threshold is percentile of all distances from any new_whale to all classified images.
        """
        scores=[]
        for img in self.id_to_img["new_whale"]:
            img_to_scores=self.Img_to_Scores(img,self.config["data"]["train_data"])
            scores+=list(iter(img_to_scores.values()))
        self.threshold=np.percentile(scores, self.confident_rate)

    def Img_to_Scores(self,img, path):     
        """
        Calculate scores of an input image wo all classified trained images.
        input: img: name of image
        output: img_to_scores: dict
        """   
    
        img=utils.img_to_tensor([img],path, transform=transform)
        with torch.no_grad():
            f0=self.branch_model(img)
        f0s=f0.repeat(self.batch_size,1)
        scores_list=[]
        
        for fs in self.data:
            fs=fs.to(self.device)
            if len(fs)!=self.batch_size:
                f0s=f0.repeat(len(fs),1)
            scores=self.head_model(f0s, fs).tolist()
            scores_list+=scores 
        img_to_scores={self.dataset.imgs[i]:scores_list[i] for i in range(len(self.dataset))}
        return img_to_scores

    def Id_to_Scores(self,img, path):
        """
        Calculate scores between an image and all other images of classified ids 
        except new_whale.
        Input: img: name of image
        Output: id_to_score: dict
        """
        img_to_score=self.Img_to_Scores(img, path)
        id_to_score={}
        for id in self.id_to_img:
            if id != "new_whale":
                id_to_score[id]=[]
                for im in self.id_to_img[id]:
                    id_to_score[id]+=[img_to_score[im]]
        return id_to_score

    def Id_to_Mean_Scores(self,img, path):
        """
        Calculate mean score between an image to images of an id.
        Input: img: name of image
        Output: dict
        """
        id_to_scores=self.Id_to_Scores(img,path)
        return {id:np.mean(id_to_scores[id]) for id in id_to_scores}

    def Predict(self,img):
        """
        Predict k possible id of an image.
        Input: img: name of image
        Output: bottom_ids: list
        """
        id_to_mean=self.Id_to_Mean_Scores(img,self.test_data)
        bottom_ids=nsmallest(self.num_top, id_to_mean, key=id_to_mean.get)
        if id_to_mean[bottom_ids[-1]]>self.threshold:
            bottom_ids[-1]="new_whale"
        return bottom_ids

    

    def Threshold_0(self):
        score=[]
        print(len(self.id_to_img))
        for id in self.id_to_img:
            if len(self.id_to_img[id])>1:
                score+=self.train.scores_matrix_modified(self.id_to_img[id]).tolist()
            
        print(score)
        self.threshold=np.percentile(score, self.confident_rate)


def prediction():
    arg_parser = argparse.ArgumentParser(description="")
    arg_parser.add_argument(
            '--config',
            default='./config.json',
            help='The Configuration file in json format')
    
    args = arg_parser.parse_args()
    config = utils.read_config(args.config)
    trainer=SiameseTrainer(config)
    model_trained=trainer.model_branch
    predictor = Predictor(config, model_trained)
    # agent.validate()    
    
    test_file=config["data"]["test_data"]
    with open(config["prediction"]["predict_csv"], "w") as f:
        f.write("Image, Whales \n")
        for img in os.listdir(test_file):
          #  img_tensor=utils.img_to_tensor([img],config["data"]["test_data"] )
            ids= predictor.Predict(img)
            f.write(f"{img}, {ids} \n")

def save_features():
    """
    Save all features.
    """
    arg_parser = argparse.ArgumentParser(description="")
    arg_parser.add_argument(
            '--config',
            default='./config.json',
            help='The Configuration file in json format')
    
    args = arg_parser.parse_args()
    config = utils.read_config(args.config)
    trainer=SiameseTrainer(config)
    model_trained=trainer.model_branch

    Img_to_Feature(config, model_trained,device='cpu')

if __name__== "__main__":
#    save_features()
    prediction()
