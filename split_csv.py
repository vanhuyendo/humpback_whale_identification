import csv
from utils import read_data
import random
import os
import shutil

def split_csv(file, num_id_train, num_id_val, train_file, val_file):
    """
    Choose some ids to train and validate.
    Input:
        file: path of original file
        num_id_train: int, number of ids to train
        num_id_val: int, number of ids to validate
        train_file: path, *.csv, to save train sample
        val_file: path, *.csv, to save val sample
    """
    id_to_img, _ = read_data(file)
    list_ids=[id for id in id_to_img if id !="new_whale"]
    random.shuffle(list_ids)
    ids_train=list_ids[:num_id_train]
    ids_val=list_ids[num_id_train:(num_id_train+num_id_val)]
    ids_to_csv(train_file, ids_train,id_to_img)
    ids_to_csv(val_file, ids_val, id_to_img)

    

def ids_to_csv(file_csv, keys, dict):
    """
    Convert a dict to csv.
    Input:
        file_csv: path, *.csv, to save file.
        keys: list, sample of keys of dict.
        dict: dict.
    """
    with open(file_csv, "w") as f:
        for key in keys:
            for img in dict[key]:
                f.write("%s, %s \n" %(img, key))
    return      


def dict_to_csv(file_csv,dict):
    """
    Convert a dict to csv.
    Input:
        file_csv: path, *csv, to save file.
        dict: dict
    """
    with open(file_csv, "w") as f:
        for key in dict.keys():
            for img in dict[key]:
                f.write("%s, %s \n" %(img, key))
    return      


def test_sample(test_origin, sample_file, n=5):
    """import 
    Sample to test the prediction.
    Input: test_origin: path folder, contains images.
            sample_file: path folder, to save images.
            n: int, number of samples to take.
    """
    count=0
    for path in os.listdir(test_origin):
        path=os.path.join(test_origin,path)
        if count<n:
            shutil.copy2(path, sample_file)
        count+=1

if __name__== "__main__":
    currentPath = "../data/"
    train_file = currentPath+"train1.csv"
    val_file = currentPath+"val1.csv"
    num_id_train=4000
    num_id_val=1000
    file=currentPath+"train.csv"

    split_csv(file, num_id_train, num_id_val, train_file, val_file)
    # test_origin=currentPath+"test"
    # sample_file=currentPath+ "test_sample/"
    # test_sample(test_origin,sample_file)



